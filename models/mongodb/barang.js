const mongoose = require('mongoose') // import mongoose
const mongoose_delete = require('mongoose-delete') // import mongoose-delete to make soft delete

// make barang schema
const BarangSchema = new mongoose.Schema({
    nama: {
        type: String,
        required: true 
    },
    harga:{
        type: mongoose.Schema.Types.Decimal128,
        required: true
    },
    pemasok: {
        type: mongoose.Schema.Types.Mixed, 
        required: true
    },
    image: {
        type: String,
        required: false,
        default: null,
        get: getImage,
        // set: value => 
    }
}, {
    //enable timestamps
    timestamps: {
        createdAt: 'created_at',
        deleteAt: 'deleted_at'
    },
    versionKey: false, //Disable__v column
    toJSON: { getters: true }
    
});

function getImage(image){
    return'/img/' + image;
}
BarangSchema.plugin(mongoose_delete, { overrideMethods: 'all' });

module.exports = barang = mongoose.model('barang', BarangSchema, 'barang');
