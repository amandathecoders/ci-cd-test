const passport = require('passport');
const auth = require('../middlewares/auth');

const express = require('express') // import express
const router = express.Router() // import router
const barangValidator = require('../middlewares/validators/barangValidator')
const BarangController = require('../controllers/barangController')

router.get('/', [passport.authenticate('barang', {
    session: false
})], BarangController.getAll)
router.get('/:id', [passport.authenticate('barang', {
    session: false
}), barangValidator.getOne], BarangController.getOne)
router.post('/create', [passport.authenticate('barang', {
    session: false
}), barangValidator.create], BarangController.create)
router.put('/update/:id', [passport.authenticate('barang', {
    session: false
}), barangValidator.update], BarangController.update) 
router.delete('/delete/:id', [passport.authenticate('barang', {
    session: false
}), barangValidator.delete], BarangController.delete)

module.exports = router; 