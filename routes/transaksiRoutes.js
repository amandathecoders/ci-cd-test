const passport = require('passport');
const auth = require('../middlewares/auth');

const express = require('express') // import express
const router = express.Router() // import router
const transaksiValidator = require('../middlewares/validators/transaksiValidator') //import transaksi validator
const TransaksiController = require('../controllers/transaksiController') // import transaksi controller

// passport.authenticate('{table/collection}', {
//     session: false
//   })  

router.get('/', [passport.authenticate('transaksi', {
    session: false
})], TransaksiController.getAll)
router.get('/:id', [passport.authenticate('transaksi', {
    session: false
}), transaksiValidator.getOne], TransaksiController.getOne)
router.post('/create', [passport.authenticate('transaksi', {
    session: false
}), transaksiValidator.create], TransaksiController.create)
router.put('/update/:id', [passport.authenticate('transaksi', {
    session: false
}), transaksiValidator.update], TransaksiController.update)
router.delete('/delete/:id', [passport.authenticate('transaksi', {
    session: false
}), transaksiValidator.delete], TransaksiController.delete)

module.exports = router; // export router