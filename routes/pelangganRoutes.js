const passport = require('passport');
const auth = require('../middlewares/auth');

const express = require('express') // import express
const router = express.Router() // import router
const pelangganValidator = require('../middlewares/validators/pelangganValidator')
const PelangganController = require('../controllers/pelangganController')

router.get('/', [passport.authenticate('pelanggan', {
    session: false
})], PelangganController.getAll)
router.get('/:id', [passport.authenticate('pelanggan', {
    session: false
}), pelangganValidator.getOne], PelangganController.getOne)
router.post('/create', [passport.authenticate('pelanggan', {
    session: false
}), pelangganValidator.create], PelangganController.create)
router.put('/update/:id', [passport.authenticate('pelanggan', {
    session: false
}),pelangganValidator.update], PelangganController.update) 
router.delete('/delete/:id', [passport.authenticate('pelanggan', {
    session: false
}), pelangganValidator.delete], PelangganController.delete)

module.exports = router; 