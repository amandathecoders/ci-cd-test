const passport = require('passport') //Import passport
const localStrategy = require('passport-local').Strategy //Import strategy
const {
  user
} = require('../../models/mysql') //IMport user model
const bcrypt = require('bcrypt') //Import bcrypt
const JWTstrategy = require('passport-jwt').Strategy //Import JWT strategy
const ExtractJWT = require('passport-jwt').ExtractJwt // Import ExtractJWT


//if user sign up
passport.use(
  'signup',
  new localStrategy({
      usernameField: 'email', //field for username from req.body.email
      passwordField: 'password', // field for password from req.body.password
      passReqToCallback: true // read other request
    },
    async (req, email, password, done) => {
      // create user data
      user.create({
        email: email, // email get from usernameField (req.body.email)
        password: password, // password get from passwordFIeld (req.body.passport)
        role: req.body.role // role get from req.body.role
      }).then(user => {
        // if sucess, it will return authorization with req.user
        return done(null, user, {
          message: 'User created'
        });
      }).catch(err => {
        // if error, it will return not authorization
        return done(null, false, {
          message: "User can't be create!"
        })
      })
    },
  )
);


//if user login
passport.use(
  'login',
  new localStrategy({
      'usernameField': 'email',
      'passwordField': 'password'
    },
    async (email, password, done) => {
      const userLogin = await user.findOne({
        where: {
          email: email
        }
      })

      if (!userLogin) {
        return done(null, false, {
          message: 'User is not found!'
        })
      }

      const validate = await bcrypt.compare(password, userLogin.password);

      if (!validate) {
        return done(null, false, {
          message: 'Wrong password !'
        })
      }

      return done(null, userLogin, {
        message: 'Login success!'
      })
    }
  )
);

// Strategy for transaction role
passport.use(
  'transaksi',
  new JWTstrategy({
      secretOrKey: 'secret_password', // key for jwt
      jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken() // extract token from authorization
    },
    async (token, done) => {
      // find user depend on token.user.email
      const userLogin = await user.findOne({
        where: {
          email: token.user.email
        }
      })
      
      // if user.role includes transaksi it will next
      if (userLogin.role.includes('transaksi')) {
        return done(null, token.user)
      }

      // if user.role not includes transaksi it will not authorization
      return done(null, false)
    }
  )
);

// Strategy for barang role
passport.use(
  'barang',
  new JWTstrategy({
      secretOrKey: 'secret_password', // key for jwt
      jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken() // extract token from authorization
    },
    async (token, done) => {
      // find user depend on token.user.email
      const userLogin = await user.findOne({
        where: {
          email: token.user.email
        }
      })

      // if user.role includes transaksi it will next
      if (userLogin.role.includes('barang')) {
        return done(null, token.user)
      }

      // if user.role not includes transaksi it will not authorization
      return done(null, false)
    }
  )
);

// Strategy for pelanggan role
passport.use(
  'pelanggan',
  new JWTstrategy({
      secretOrKey: 'secret_password', // key for jwt
      jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken() // extract token from authorization
    },
    async (token, done) => {
      // find user depend on token.user.email
      const userLogin = await user.findOne({
        where: {
          email: token.user.email
        }
      })

      // if user.role includes transaksi it will next
      if (userLogin.role.includes('pelanggan')) {
        return done(null, token.user)
      }

      // if user.role not includes transaksi it will not authorization
      return done(null, false)
    }
  )
);

// Strategy for pemasok role
passport.use(
  'pemasok',
  new JWTstrategy({
      secretOrKey: 'secret_password', // key for jwt
      jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken() // extract token from authorization
    },
    async (token, done) => {
      // find user depend on token.user.email
      const userLogin = await user.findOne({
        where: {
          email: token.user.email
        }
      })

      // if user.role includes transaksi it will next
      if (userLogin.role.includes('pemasok')) {
        return done(null, token.user)
      }

      // if user.role not includes transaksi it will not authorization
      return done(null, false)
    }
  )
);